#!/usr/bin/env bash

git push origin master
remote() {
    echo "running: $1"
    REPO='$HOME/determiner-priming-psiturk/'
    ssh ec2-user@54.210.157.79 -i $SPOT_MICRO_PEM "cd $REPO; $1"
}

git push origin master
remote 'git fetch origin' &&
remote 'git checkout staging' &&
remote 'git merge origin/master' &&
remote 'browserify static/js/task.js -o static/js/bundle.js'
remote 'browserify static/js/consent.js -o static/js/consent-bundle.js'
