from pprint import pprint
from json import dumps
import re
from sys import argv

infile = argv[1]
data = open(infile).readlines()
seqs = []
images = None
for line in data:
    line = re.sub('\t+', '\t', line.strip())
    parts = line.split('\t')[2:]
    word_type = parts[0]
    fns = parts[1:5]
    fns = list(map(lambda x: x.lstrip('Images\\'), fns))
    images = fns
    data_dict = {'type': word_type,
                 'sentence': parts[5],
                 'correctResponse': parts[6]}
    seqs.append(data_dict)
allData = {'images': images,
           'sequences': seqs}
print(dumps(allData))
