from pprint import pprint
from json import dumps
import re
from sys import argv

infile = argv[1]
data = open(infile).readlines()
seqs = []
images = None

def strip_image(img):
    return img.lstrip('Images\\')

for n, line in enumerate(data):
    if n % 3 == 0:
        continue
    line = re.sub('\t+', '\t', line.strip())
    parts = line.split('\t')[2:]
    prime_type = parts[0]
    prime = {}
    prime['sentences'] = parts[1:3]
    prime['question'] = parts[3]
    prime['image'] = strip_image(parts[4])
    target = {}
    target['question'] = parts[5]
    target['image'] = strip_image(parts[6])
    data_dict = {'type': prime_type,
                 'prime': prime,
                 'target': target}
    seqs.append(data_dict)
print(dumps(seqs))
