trap "exit" SIGINT SIGTERM
while true
do
    browserify task.js -o bundle.js;
    browserify consent.js -o consent-bundle.js;
    inotifywait *.js **/*.js -e modify;
done
