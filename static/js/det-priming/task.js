/*global require, module, performance */

'use strict';

var _ = require('underscore'),
    Promise = require('es6-promise').Promise,
    data = require('./data'),
    utils = require('../utils'),
    wait = utils.wait,
    showFixation = utils.showFixation,
    waitForClick = utils.waitForClick,
    log = _.partial(utils.log, 'DETPRIME');

var imagePath = '/static/images/det-priming/';
var getImage = function(fn){
    log('getImage()', fn);
    return imagePath.concat(fn);
};

// driver :: psiTurk-compatible experiment driver obj
// condition :: string identifying condition name
var detprime = function(driver, condition){
    log('detprime()');
    if (typeof condition === 'undefined'){
        throw Error('condition name required');
    }
    if (!_.contains(['trial1', 'trial2', 'control'], condition)){
        throw Error('invalid condition: '.concat(condition));
    };

    utils.preloadImages('/static/images/det-priming/', data);
    driver.preloadPages(
        utils.prefix('det-priming/',
                     ['prime-frame.html',
                      'collect-frame.html',
                      'collect-frame-single-row.html',
                      'prime-warmup.html',
                      'instructions-lexical-warmup.html',
                      'instructions-priming.html',
                      'instructions-target-control.html',
                      'instructions-target-experimental.html',
                      'instructions-trial-prompt.html',
                      'instructions-end.html']));

    var fixation = _.partial(showFixation, driver);
    var run = function(){
        log('run()');
        return runExamples().then(runTrials);
    };

    var runExamples = function(){
        var showPage = _.bind(driver.showPage, driver);
        var page = function(path) {
            return function(){
                showPage(path);
                return waitForClick('#next')();
            };
        };
        var fixation = _.partial(showFixation, driver),
            okay = _.partial(fixation, 'Okay!');

        var control = utils.pseq(
            // 1
            page('det-priming/instructions-lexical-warmup.html'),
            lexicalWarmup(driver, data.warmupExample, true),

            // 2
            page('det-priming/instructions-target-control.html'),
            promptTargetResponse(driver, 'Tommy is...', 'Tommysleeping.jpg'),
            function(args){
                    driver.recordTrialData({
                        section: 'determiner priming',
                        phase: 'practice',
                        list: condition,
                        target: ['Tommy is...', 'Tommysleeping.jpg'],
                        targetResponse: args.targetResponse,
                        targetRT: args.reactionTime,
                        condition: 'control'
                    });
                    driver.saveData();
            },
            okay,

            page('det-priming/instructions-trial-prompt.html'),
            lexicalWarmup(driver, data.warmupPractice, false),
            promptTargetResponse(driver, "Katie is ...", "Katieplayingblocks.jpg"),

            function(args){
                driver.recordTrialData({
                    section: 'determiner priming',
                    phase: 'practice',
                    list: condition,
                    target: [data.primeExample.target.question,
                             data.primeExample.target.image],
                    targetResponse: args.targetResponse,
                    targetRT: args.reactionTime,
                    condition: 'control'
                });
                driver.saveData();
            },
            page('det-priming/instructions-end.html')
        );

        var experimental = utils.pseq(
            page('det-priming/instructions-lexical-warmup.html'),
            lexicalWarmup(driver, data.warmupExample, true),

            page('det-priming/instructions-priming.html'),
            showPrime(driver, "Katiehavingdinner.jpg", [
                "Look at Katie. She is eating.",
                "Katie is having dinner"
            ]),
            promptPrimeResponse(driver, "Katie is..."),

            function(response){
                log('priming', ':priming task', 'got response', response);
                driver.recordTrialData({
                    section: 'determiner priming',
                    phase: 'practice',
                    list: condition,
                    // TODO: when should this be practice?
                    primeType: 'practice',
                    primePrompts: ["Katiehavingdinner.jpg", [ "Look at Katie. She is eating.",
                                                              "Katie is having dinner" ]
                                  ],
                    primeSentence: "Katie is...",
                    primeResponse: response,
                    condition: 'priming'
                });
                driver.saveData();
            },
            okay,

            // target instructions
            page('det-priming/instructions-target-experimental.html'),
            promptTargetResponse(driver, 'Tommy is ...', 'Tommysleeping.jpg'),

            function(args){
                    driver.recordTrialData({
                        section: 'determiner priming',
                        phase: 'practice',
                        list: condition,
                        target: ["Tommy is...",
                                 "Tommysleeping.jpg"],
                        targetResponse: args.targetResponse,
                        targetRT: args.reactionTime,
                        condition: 'priming'
                    });
                    driver.saveData();
            },
            okay,

            page('det-priming/instructions-trial-prompt.html'),
            lexicalWarmup(driver, data.warmupPractice, false),

            // practice set - 3, 4
            primingTask(driver, 'priming', 'practice', {
                "target" : {
                    "question" : "Katie is ...",
                    "image" : "Katieplayingblocks.jpg"
                },
                "prime" : {
                    "question" : "Tommy is ...",
                    "sentences" : [
                        "Here is Tommy. He is walking.",
                        "Tommy is walking outside."
                    ],
                    "image" : "Tommywalkingoutside.jpg"
                }
            }),

            page('det-priming/instructions-end.html')
        );

        if (condition === 'control'){
            return control();
        } else {
            return experimental();
        };
    };

    var runTrials = function(){
        log('runTrials()');
        var trials = data[condition],
            priming = _.partial(primingTask, driver, condition, 'trial'),
            warmup = _.partial(lexicalWarmup, driver),
            parts = utils.pseq(
                warmup(data.warmup1),
                priming(trials[0]),
                priming(trials[1]),
                warmup(data.warmup2),
                priming(trials[2]),
                priming(trials[3]),
                warmup(data.warmup3),
                priming(trials[4]),
                priming(trials[5]),
                warmup(data.warmup4),
                priming(trials[6]),
                priming(trials[7]));

        return parts();
    };

    return {
        run: run
    };

};

var showPrime = function(driver, image, sentences){
    return function(){
        log('showPrime()');
        driver.showPage('det-priming/prime-frame.html');
        $('.image').attr('src', getImage(image));
        $('.top-caption').text(sentences[0]);
        return wait(3000)()
            .then(function(){
                $('.top-caption').text('');
                $('.bottom-caption').text(sentences[1]);
            })
            .then(wait(3000));
    };
};

var promptPrimeResponse = function(driver, question){
    return function(){
        log('priming', 'promptPrimeResponse()');
        driver.showPage('det-priming/collect-frame-single-row.html');
        $('[autofocus]:first').focus();
        $('.prompt-sentence').text(question);
        $('#response').focus();
        return new Promise(function(resolve, reject){
            $('#response').bind('keypress', function(event){
                var response = $('#response').val();
                if(event.keyCode == 13 && response !== ''){
                    resolve(response);
                }
            });
        });
    };
};

var promptTargetResponse = function(driver, question, image){
    return function(){
        var startTime;
        log('priming', 'promptTargetResponse()');
        driver.showPage('det-priming/collect-frame.html');
        $('[autofocus]:first').focus();
        startTime = performance.now();
        $('.prompt-sentence').text(question);
        $('.target-image').attr('src', getImage(image));
        $('#response').focus();
        return new Promise(function(resolve, reject){
            $('#response').bind('keypress', function(event){
                var response = $('#response').val();
                if(event.keyCode == 13 && response !== ''){
                    resolve({
                        targetResponse: response,
                        reactionTime: performance.now() - startTime
                    });
                }
            });
        });
    };
};

// In the experimental condition, shows a priming image with text above and
// below it, then prompts the user for a response. A target image is then
// displayed and directly below it a prompt is shown and a response recorded.
// In the control condition only the target image and prompt are presented and a
// response collected.
var primingTask = function(driver, condition, phase, data){
    log('priming', 'init');
    var primeResponse;
    return function(){
        if(condition == 'control'){
            log('priming', ':priming task:', 'skipping');
            return promptTargetResponse(driver, data.target.question,
                                        data.target.image)()
                .then(function(args){
                    log('args', args);
                    driver.recordTrialData({
                        section: 'determiner priming',
                        phase: phase,
                        list: condition,
                        target: [data.target.question, data.target.image],
                        targetResponse: args.targetResponse,
                        targetRT: args.reactionTime,
                        condition: (condition == 'control' ?
                                    'control' : 'priming')
                    });
                    driver.saveData();
                });
        }
        else {
            var fixation = _.partial(showFixation, driver);
            log('priming', ':priming task:', 'starting');

            return fixation()
                .then(showPrime(driver, data.prime.image, data.prime.sentences))
                .then(promptPrimeResponse(driver, data.prime.question))
                .then(function(response){
                    log('priming', ':priming task', 'got response', response);
                    primeResponse = response;
                })
                .then(fixation)
                .then(promptTargetResponse(driver, data.target.question,
                                           data.target.image))
                .then(function(args){
                    log('priming', ':priming task', 'got response', args);
                    driver.recordTrialData({
                        section: 'determiner priming',
                        phase: phase,
                        list: condition,
                        // TODO: when should this be practice?
                        primeType: data.type,
                        primePrompts: data.prime.sentences.concat(data.prime.image),
                        primeSentence: data.prime.question,
                        primeResponse: primeResponse,
                        target: [data.target.question, data.target.image],
                        targetResponse: args.targetResponse,
                        targetRT: args.reactionTime,
                        condition: (condition == 'control' ?
                                    'control' : 'priming')
                    });
                    driver.saveData();
                });
        }
    };
};

// Displays a four-quadrant grid of images and prompts the user to click on
// different ones based on textual prompts that describe some activity or item
// in the picture. Does not record data.
var lexicalWarmup = function(driver, warmupData, shouldShowFeedback){
    if (typeof shouldShowFeedback === 'undefined'){
        shouldShowFeedback = false;
    }
    log('lexicalWarmup', 'priming', 'starting');
    var showCaption = function(text){
        $('.bottom-caption').text(text);
    };

    var collectResponse = function(){
        log('lexicalWarmup', 'collectResponse()');
        return new Promise(function(resolve, reject){
            $('.response').bind('mousedown', function(event){
                var quadrant = event.target.dataset['quadrant'];
                log('lexicalWarmup', 'collectResponse', 'gotresponse', quadrant);
                $('.response').unbind('mousedown');
                resolve(quadrant);
            });
        });
    };

    var showFeedback = function(str){
        $('img').hide();
        $('.bottom-caption').text('');
        $('.feedback').text(str);
        return wait(1000)().then(function(){
            $('img').show();
            $('.feedback').text('');
        });
    };

    var next = function(sequence){
        log('lexicalWarmup', 'next()');
        if (!sequence.length){
            log('lexicalWarmup', ':lexical warmup', 'finished');
            return sequence;
        }
        var data = sequence[0];

        showCaption(data.sentence);
        return collectResponse()
            .then(function(response){
                var correct = response == data.correctResponse;
                if (correct && !shouldShowFeedback){
                    return next(sequence.slice(1));
                } else if (correct && shouldShowFeedback){
                    return showFeedback('Okay!').then(_.partial(next,
                                                                sequence.slice(1)));
                } else {
                    return showFeedback('Try Again!').then(_.partial(next,
                                                                     sequence));
                }
            });
    };

    var setupPage = function(){
        log('lexicalWarmup', 'setupPage()');
        var images = warmupData.images;
        driver.showPage('det-priming/prime-warmup.html');
        // TODO: q1, q2, q3, q4 should be remapped to
        //       q2, q1, q3, q4
        $('.q1').attr('src', getImage(images[0]));
        $('.q2').attr('src', getImage(images[1]));
        $('.q3').attr('src', getImage(images[2]));
        $('.q4').attr('src', getImage(images[3]));
    };

    return function(){
        log('lexicalWarmup', 'run()', 'starting');
        return showFixation(driver)
            .then(setupPage)
            .then(_.partial(next, warmupData.sequences));
    };
};



module.exports = {
    task: detprime
};
