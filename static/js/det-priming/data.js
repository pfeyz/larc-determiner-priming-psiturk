/*global module */

module.exports = {
    warmupExample: {
        "images":  [
            "Tommysleeping.jpg",
            "Katiehavingdinner.jpg",
            "Tommywalkingoutside.jpg",
            "Katieplayingblocks.jpg"
        ],
        "sequences": [
            {
                "sentence" : "Show me dinner.",
                "type" : "",
                "correctResponse" : "2"
            },
            {
                "sentence" : "Show me blocks.",
                "type" : "",
                "correctResponse" : "4"
            },
            {
                "sentence" : "Show me sleeping.",
                "type" : "",
                "correctResponse" : "1"
            },
            {
                "sentence" : "Show me walking.",
                "type" : "",
                "correctResponse" : "3"
            }
        ]
    },
    primeExample: {
        "target" : {
            "question" : "Katie is ...",
            "image" : "Katieplayingblocks.jpg"
        },
        "prime" : {
            "question" : "Tommy is ...",
            "sentences" : [
                "Here is Tommy. He is walking.",
                "Tommy is walking outside."
            ],
            "image" : "Tommywalkingoutside.jpg"
        }
    },

    targetListExample: {
            "target": {
                "question": "Katie is ...",
                "image": "Katieplayingblocks.jpg"
            }
        },

    warmupPractice: {
        images: [
            "Tommysleeping.jpg",
            "Katiehavingdinner.jpg",
            "Tommywalkingoutside.jpg",
            "Katieplayingblocks.jpg"
        ],
        sequences: [
            {"sentence":"Show me trees.",
             "type":"Noun",
             "correctResponse":"3"},
            {"sentence":"Show me blankets.",
             "type":"Noun",
             "correctResponse":"1"},
            {"sentence":"Show me playing.",
             "type":"Verb",
             "correctResponse": "4"},
            {"sentence":"Show me eating.",
             "type":"Verb",
             "correctResponse":"2"}
        ]
    },

    targetListPractice: {
        prime: {
            question: "Katie is...",
            image: "Katiehavingdinner.jpg",
            sentences: [
                "Look at Katie",
                "Katie is having dinner"
            ]
        }
    },

    warmup1: {
        "images" : [
            "Katiebreakingcrayons.jpg",
            "Tommyreadingbooks.jpg",
            "Katieturningwheels.jpg",
            "Tommypickingflowers.jpg"
        ],
        "sequences" : [
            {
                "sentence" : "Show me wheels.",
                "type" : "Noun",
                "correctResponse" : "3"
            },
            {
                "sentence" : "Show me crayons.",
                "type" : "Noun",
                "correctResponse" : "1"
            },
            {
                "sentence" : "Show me books.",
                "type" : "Noun",
                "correctResponse" : "2"
            },
            {
                "sentence" : "Show me flowers.",
                "type" : "Noun",
                "correctResponse" : "4"
            },
            {
                "sentence" : "Show me picking.",
                "type" : "Verb",
                "correctResponse" : "4"
            },
            {
                "sentence" : "Show me reading.",
                "type" : "Verb",
                "correctResponse" : "2"
            },
            {
                "sentence" : "Show me turning.",
                "type" : "Verb",
                "correctResponse" : "3"
            },
            {
                "sentence" : "Show me breaking.",
                "type" : "Verb",
                "correctResponse" : "1"
            }
        ]
    },

    warmup2: {
        "images" : [
            "Tommycookingfood.jpg",
            "Katiepushingcars.jpg",
            "Tommydrawingpictures.jpg",
            "Katieholdingjuice.jpg"
        ],
        "sequences" : [
            {
                "sentence" : "Show me cars.",
                "type" : "Noun",
                "correctResponse" : "2"
            },
            {
                "sentence" : "Show me pictures.",
                "type" : "Noun",
                "correctResponse" : "3"
            },
            {
                "sentence" : "Show me juice.",
                "type" : "Noun",
                "correctResponse" : "4"
            },
            {
                "sentence" : "Show me food.",
                "type" : "Noun",
                "correctResponse" : "1"
            },
            {
                "sentence" : "Show me cooking.",
                "type" : "Verb",
                "correctResponse" : "1"
            },
            {
                "sentence" : "Show me drawing.",
                "type" : "Verb",
                "correctResponse" : "3"
            },
            {
                "sentence" : "Show me pushing.",
                "type" : "Verb",
                "correctResponse" : "2"
            },
            {
                "sentence" : "Show me holding.",
                "type" : "Verb",
                "correctResponse" : "4"
            }
        ]
    },

    warmup3: {
        "images" : [
            "Katieclimbingstairs.jpg",
            "Tommyeatinggrapes.jpg",
            "Katiewashingsocks.jpg",
            "Tommypullingtrains.jpg"
        ],
        "sequences" : [
            {
                "sentence" : "Show me stairs.",
                "type" : "Noun",
                "correctResponse" : "1"
            },
            {
                "sentence" : "Show me socks.",
                "type" : "Noun",
                "correctResponse" : "3"
            },
            {
                "sentence" : "Show me trains.",
                "type" : "Noun",
                "correctResponse" : "4"
            },
            {
                "sentence" : "Show me grapes.",
                "type" : "Noun",
                "correctResponse" : "2"
            },
            {
                "sentence" : "Show me washing.",
                "type" : "Verb",
                "correctResponse" : "3"
            },
            {
                "sentence" : "Show me pulling.",
                "type" : "Verb",
                "correctResponse" : "4"
            },
            {
                "sentence" : "Show me climbing.",
                "type" : "Verb",
                "correctResponse" : "1"
            },
            {
                "sentence" : "Show me eating.",
                "type" : "Verb",
                "correctResponse" : "2"
            }
        ]
    },

    warmup4: {
        "images" : [
            "Tommydrinkingwater.jpg",
            "Katiepaintingeggs.jpg",
            "Tommyfixingtrucks.jpg",
            "Katiecuttingpaper.jpg"
        ],
        "sequences" : [
            {
                "sentence" : "Show me water.",
                "type" : "Noun",
                "correctResponse" : "1"
            },
            {
                "sentence" : "Show me eggs.",
                "type" : "Noun",
                "correctResponse" : "2"
            },
            {
                "sentence" : "Show me trucks.",
                "type" : "Noun",
                "correctResponse" : "3"
            },
            {
                "sentence" : "Show me paper.",
                "type" : "Noun",
                "correctResponse" : "4"
            },
            {
                "sentence" : "Show me cutting.",
                "type" : "Verb",
                "correctResponse" : "4"
            },
            {
                "sentence" : "Show me fixing.",
                "type" : "Verb",
                "correctResponse" : "3"
            },
            {
                "sentence" : "Show me drinking.",
                "type" : "Verb",
                "correctResponse" : "1"
            },
            {
                "sentence" : "Show me painting.",
                "type" : "Verb",
                "correctResponse" : "2"
            }
        ]
    },

    trial1: [
        {
            "target" : {
                "question" : "Tommy is ...",
                "image" : "Tommyreadingbooks.jpg"
            },
            "prime" : {
                "question" : "Katie is ...",
                "sentences" : [
                    "Look at her crayons. They are blue.",
                    "Katie is breaking her crayons."
                ],
                "image" : "Katiebreakingcrayons.jpg"
            },
            "type" : "Determiner"
        },
        {
            "target" : {
                "question" : "Tommy is ...",
                "image" : "Tommypickingflowers.jpg"
            },
            "prime" : {
                "question" : "Katie is ...",
                "sentences" : [
                    "Look at those wheels. They are green.",
                    "Katie is turning those wheels."
                ],
                "image" : "Katieturningwheels.jpg"
            },
            "type" : "Determiner"
        },
        {
            "target" : {
                "question" : "Katie is ...",
                "image" : "Katiepushingcars.jpg"
            },
            "prime" : {
                "question" : "Tommy is ...",
                "sentences" : [
                    "Look at that food. It looks nice.",
                    "Tommy is cooking nice food."
                ],
                "image" : "Tommycookingfood.jpg"
            },
            "type" : "Adjective"
        },
        {
            "target" : {
                "question" : "Katie is ...",
                "image" : "Katieholdingjuice.jpg"
            },
            "prime" : {
                "question" : "Tommy is ...",
                "sentences" : [
                    "Here are some pictures. They are big.",
                    "Tommy is drawing some pictures."
                ],
                "image" : "Tommydrawingpictures.jpg"
            },
            "type" : "Determiner"
        },
        {
            "target" : {
                "question" : "Tommy is ...",
                "image" : "Tommyeatinggrapes.jpg"
            },
            "prime" : {
                "question" : "Katie is ...",
                "sentences" : [
                    "Here are some stairs. They are big.",
                    "Katie is climbing big stairs."
                ],
                "image" : "Katieclimbingstairs.jpg"
            },
            "type" : "Adjective"
        },
        {
            "target" : {
                "question" : "Tommy is ...",
                "image" : "Tommypullingtrains.jpg"
            },
            "prime" : {
                "question" : "Katie is ...",
                "sentences" : [
                    "Look at her socks. They are green.",
                    "Katie is washing green socks."
                ],
                "image" : "Katiewashingsocks.jpg"
            },
            "type" : "Adjective"
        },
        {
            "target" : {
                "question" : "Katie is ...",
                "image" : "Katiepaintingeggs.jpg"
            },
            "prime" : {
                "question" : "Tommy is ...",
                "sentences" : [
                    "Look at that water. It looks nice.",
                    "Tommy is drinking that water."
                ],
                "image" : "Tommydrinkingwater.jpg"
            },
            "type" : "Determiner"
        },
        {
            "target" : {
                "question" : "Katie is ...",
                "image" : "Katiecuttingpaper.jpg"
            },
            "prime" : {
                "question" : "Tommy is ...",
                "sentences" : [
                    "Look at those trucks. They are blue.",
                    "Tommy is fixing blue trucks."
                ],
                "image" : "Tommyfixingtrucks.jpg"
            },
            "type" : "Adjective"
        }
    ],

    trial2: [
        {
            "target" : {
                "question" : "Tommy is ...",
                "image" : "Tommyreadingbooks.jpg"
            },
            "prime" : {
                "question" : "Katie is ...",
                "sentences" : [
                    "Look at her crayons. They are blue.",
                    "Katie is breaking blue crayons."
                ],
                "image" : "Katiebreakingcrayons.jpg"
            },
            "type" : "Adjective"
        },
        {
            "target" : {
                "question" : "Tommy is ...",
                "image" : "Tommypickingflowers.jpg"
            },
            "prime" : {
                "question" : "Katie is ...",
                "sentences" : [
                    "Look at those wheels. They are green.",
                    "Katie is turning green wheels."
                ],
                "image" : "Katieturningwheels.jpg"
            },
            "type" : "Adjective"
        },
        {
            "target" : {
                "question" : "Katie is ...",
                "image" : "Katiepushingcars.jpg"
            },
            "prime" : {
                "question" : "Tommy is ...",
                "sentences" : [
                    "Look at that food. It looks nice.",
                    "Tommy is cooking that food."
                ],
                "image" : "Tommycookingfood.jpg"
            },
            "type" : "Determiner"
        },
        {
            "target" : {
                "question" : "Katie is ...",
                "image" : "Katieholdingjuice.jpg"
            },
            "prime" : {
                "question" : "Tommy is ...",
                "sentences" : [
                    "Here are some pictures. They are big.",
                    "Tommy is drawing big pictures."
                ],
                "image" : "Tommydrawingpictures.jpg"
            },
            "type" : "Adjective"
        },
        {
            "target" : {
                "question" : "Tommy is ...",
                "image" : "Tommyeatinggrapes.jpg"
            },
            "prime" : {
                "question" : "Katie is ...",
                "sentences" : [
                    "Here are some stairs. They are big.",
                    "Katie is climbing some stairs."
                ],
                "image" : "Katieclimbingstairs.jpg"
            },
            "type" : "Determiner"
        },
        {
            "target" : {
                "question" : "Tommy is ...",
                "image" : "Tommypullingtrains.jpg"
            },
            "prime" : {
                "question" : "Katie is ...",
                "sentences" : [
                    "Look at her socks. They are green.",
                    "Katie is washing her socks."
                ],
                "image" : "Katiewashingsocks.jpg"
            },
            "type" : "Determiner"
        },
        {
            "target" : {
                "question" : "Katie is ...",
                "image" : "Katiepaintingeggs.jpg"
            },
            "prime" : {
                "question" : "Tommy is ...",
                "sentences" : [
                    "Look at that water. It looks nice.",
                    "Tommy is drinking nice water."
                ],
                "image" : "Tommydrinkingwater.jpg"
            },
            "type" : "Adjective"
        },
        {
            "target" : {
                "question" : "Katie is ...",
                "image" : "Katiecuttingpaper.jpg"
            },
            "prime" : {
                "question" : "Tommy is ...",
                "sentences" : [
                    "Look at those trucks. They are blue.",
                    "Tommy is fixing those trucks."
                ],
                "image" : "Tommyfixingtrucks.jpg"
            },
            "type" : "Determiner"
        }
    ],

    control: [
        {
            "target": {
                "question": "Tommy is ...",
                "image": "Tommyreadingbooks.jpg"
            }
        },
        {
            "target": {
                "question": "Tommy is ...",
                "image": "Tommypickingflowers.jpg"
            }
        },
        {
            "target": {
                "question": "Katie is ...",
                "image": "Katiepushingcars.jpg"
            }
        },
        {
            "target": {
                "question": "Katie is ...",
                "image": "Katieholdingjuice.jpg"
            }
        },
        {
            "target": {
                "question": "Tommy is ...",
                "image": "Tommyeatinggrapes.jpg"
            }
        },
        {
            "target": {
                "question": "Tommy is ...",
                "image": "Tommypullingtrains.jpg"
            }
        },
        {
            "target": {
                "question": "Katie is ...",
                "image": "Katiepaintingeggs.jpg"
            }
        },
        {
            "target": {
                "question": "Katie is ...",
                "image": "Katiecuttingpaper.jpg"
            }
        }
    ]};
