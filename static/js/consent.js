/*global psiTurk, require, expUrl, $, _ */
'use strict';

var utils = require('./utils'),
    Promise = require('es6-promise').Promise,
    waitForClick = utils.waitForClick;

// copied from psiTurk
var pages = {},
    pagenames = [ 'consent-form-intro.html',
                  'activity-a.html',
                  'activity-c.html',
                  'activity-j.html',
                  'consent-written-response.html',
                  'consent-form-final.html' ],
    // copied from psiTurk
    getPage = function(name) {
	if (!(name in pages)){
	    throw new Error(
		["Attemping to load page before preloading: ",
		 name].join(""));
	};
	return pages[name];
    },
    replaceBody = function(x) { $('#content').html(x); },
    showPage = function(name){
        replaceBody(getPage(name));
        var buttonSection = $('#button-container'),
            html = buttonSection.html();
        buttonSection.children().remove();
        $('#button-container').append(html);
    };

_.each(pagenames, function(page) {
    $.ajax({
	url: '/consent/' + page,
	success: function(page_html) { pages[page] = page_html;},
	dataType: "html",
	async: false
    });
});

var allChecked = function(inputSelector, buttonSelector){
    return function(){
        var inputs = $(inputSelector);
        var button = $(buttonSelector).prop('disabled', true);
        return new Promise(function(resolve, reject){
            button.on('click', resolve);
            inputs.on('click', function(){
                var unchecked = _.findWhere(inputs, {checked: false});
                if (typeof unchecked === 'undefined'){
                    button.prop('disabled', false);
                } else {
                    button.prop('disabled', true);
                }
            });
        });
    };
};

$(window).load(function(){
    var page = function(name){
        return _.partial(showPage, name);
    };
    showPage(pagenames[0]);
    var consentList = [], // the list of items the user consented to to
        readConsent = function(){
            var nums = _.map($('input[type="checkbox"]:checked'),
                             function(el){
                                 return el.dataset['consentNum'];
                             });
            consentList = consentList.concat(nums);
        };
    waitForClick('button.continue')()
        .then(page('activity-a.html'))
        .then(waitForClick('button.continue'))
        .then(page('activity-c.html'))
        .then(waitForClick('button.continue'))
        .then(page('activity-j.html'))
        .then(waitForClick('button.continue'))
        .then(page('consent-form-final.html'))
        .then(allChecked('.mandatory', 'button.continue'))
        .then(readConsent)
        .then(page('consent-written-response.html'))
        .then(allChecked('.mandatory', 'button.continue'))
        .then(readConsent)
        .then(function(){
            expUrl += '&consents=' + consentList.join('');
            window.location = expUrl;
        });
});
