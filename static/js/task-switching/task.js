/*global require, module, performance */
'use strict';

var _ = require('underscore'),
    Promise = require('es6-promise').Promise,
    utils = require('../utils'),
    P = utils.P,
    log = utils.log,
    wait = utils.wait,
    waitForClick = utils.waitForClick;

var taskswitch = function(driver, inverted){
    if (typeof inverted === 'undefined'){
        throw Error('inversion condition name required');
    }
    if (!_.contains([true, false], inverted)){
        throw Error('invalid inversion condition: '.concat(inverted));
    };

    // TODO: randomize over blocks of 16 items
    var generateData = function(){
        var trialTypes = [];
        var trialNums = [1,2,3,4,
                         6,7,8,9];

        for (var i = 0; i < 6; i++){
            var block = [];
            for (var j = 0; j < 8; j++){
                block.push('size');
                block.push('parity');
            }
            trialTypes = trialTypes.concat(_.shuffle(block));
        }

        return _.map(trialTypes, function(type){
            return [type, _.sample(trialNums)];
        });
    };

    var run = function(){
        var data = generateData();
        log('starting taskSwitch');
        driver.preloadPages(['task-switch-frame.html',
                             'task-switching/instructions-parity.html',
                             'task-switching/instructions-size.html',
                             'task-switching/instructions-final.html']);
        return instructions(inverted)
            .then(_.partial(trials, data, inverted, 'experimental'));
    };

    var KEYS = (function(){
        var c = function(x){ return x.charCodeAt(0); };
        return {
            leftA:  c('a'),
            leftB:  c('s'),
            rightA: c('k'),
            rightB: c('l')
        };
    })();

    var getKeyMappings = function(inverted){
        var keyMappings = {},
            lefts = ['odd', 'even'],
            rights = ['small', 'big'];
        if (inverted){
            // swap elements
            lefts.unshift(lefts.pop());
            rights.unshift(rights.pop());
        }
        keyMappings[KEYS.leftA] = lefts[0];
        keyMappings[KEYS.leftB] = lefts[1];
        keyMappings[KEYS.rightA] = rights[0];
        keyMappings[KEYS.rightB] = rights[1];
        return keyMappings;
    };

    // showFixation :: () -> ()
    var showFixation = function(){
        return new Promise(function(resolve, reject){
            driver.showPage('fixation.html');
            _.delay(resolve, 500);
        });
    };

    // stims = [condition, number]
    var showStimGetResponse = function(stims, inverted){
        var condition, number, keyMappings, labels, startTime;
        log('showStim()');
        condition = stims[0];
        number = stims[1];
        keyMappings = getKeyMappings(inverted);
        return function(){
            return new Promise(function(resolve, reject){
                log('showStim()()');
                if (condition == 'parity'){
                    labels = [
                        keyMappings[KEYS.leftA],
                        keyMappings[KEYS.leftB]
                    ];

                } else if (condition == 'size'){
                    labels = [
                        keyMappings[KEYS.rightA],
                        keyMappings[KEYS.rightB]
                    ];
                } else {
                    throw Error(['Invalid condition type:', condition]
                                .join(' '));
                }

                driver.showPage('task-switch-frame.html');
                $('.target-num').text(number);
                $('.task-type').text(labels.join('/').toUpperCase());
                startTime = performance.now();

                $('body').bind('keypress', function(event){
                    var response = keyMappings[event.charCode];
                    if (typeof response !== 'undefined'){
                        var now = performance.now();
                        $('body').unbind('keypress');
                        resolve({
                            condition: condition,
                            inverted: inverted,
                            stimulus: number,
                            keyMappings: keyMappings,
                            startTime: startTime,
                            reactionTime: now - startTime,
                            response: response
                        });
                    }
                });
            });
        };
    };

    // judgeParity :: Int -> Str -> Bool | Error
    var judgeParity = function(stim, response){
        return (stim % 2 == 0) ? 'even' : 'odd';
    };

    // judgeSize :: Int -> Str -> Bool | Error
    var judgeSize = function(stim, response){
        return (stim < 5) ? 'small' : 'big';
    };

    // judgeResponse :: {condition :: Str, stimulus :: Int, response :: Str}
    //                  -> Bool | Error
    var judgeResponse = function(args){
        var reactionTime, correctResponse, accuracy;

        var condition = args.condition,
            stimulus = args.stimulus,
            response = args.response,
            inverted = args.inverted;

        if (stimulus == 5 || stimulus < 1 || stimulus > 10){
            throw Error(['Invalid stimulus:', stimulus].join(' '));
        }

        if (!_.contains(['big', 'small', 'even', 'odd'], response)){
            throw Error(['invalid response:', response]
                        .join(' '));
        }

        if (condition == 'size') {
            correctResponse = judgeSize(stimulus, response);
        } else if (condition == 'parity'){
            correctResponse = judgeParity(stimulus, response);
        } else {
            throw Error(['Invalid condition', condition].join(''));
        }
        accuracy = correctResponse == response;
        return _.extend(args, {
            accuracy: accuracy,
            correctResponse: correctResponse
        });
    };

    // recordData :: {condition :: String, stimulus :: Int, response :: Str}
    var recordData = function(opts){
        return function(args){
            args = _.extend(args, opts);
            // TODO: why are response and keymappings ending up in an sub-obj in
            //       the trialdata key?
            driver.recordTrialData([args]);
            driver.saveData();
            log(args);
            return args;
        };
    };

    var showFeedback = function(args){
        var feedback =  args.accuracy === true ? 'correct' : 'incorrect';
        $('.target-num').text('');
        $('.task-type').text('');
        $('.feedback').text(feedback);
        return Promise.resolve(args)
            .then(wait(1000));
    };


    var renderTemplate = function(text, replacements){
        _.each(_.keys(replacements), function(key){
            var target = key,
                replacement = replacements[key];
            text = text.replace(new RegExp('{' + target + '}', 'g'),
                                replacement);
        });
        return text;
    };
    var instructions = function(inverted){
        var page = function(path, params) {
            return function() {
                driver.showPage(path);
                _.each($('p'), function(p, n){
                    $(p).text(renderTemplate($(p).text(), params));
                });
                return true;
            };
        };
        var opt = function(x, y) { return inverted ? y : x; },
            parityParams = {
                taskName: opt('ODD/EVEN','EVEN/ODD'),
                left    : opt('odd' ,'even'),
                right   : opt('even' ,'odd'),
                leftKey : 'A',
                rightKey: 'S'
            },
            sizeParams = {
                taskName: opt('SMALL/BIG', 'BIG/SMALL'),
                left    : opt('smaller', 'bigger'),
                right   : opt('bigger', 'smaller'),
                leftKey : 'K',
                rightKey: 'L'
            };

        var finalParams = {
            parityLeft: parityParams.left.toUpperCase(),
            parityRight: parityParams.right.toUpperCase(),
            parityLeftKey: parityParams.leftKey,
            parityRightKey: parityParams.rightKey,
            sizeLeft: opt('SMALL', 'BIG'),
            sizeRight: opt('BIG', 'SMALL'),
            sizeLeftKey: sizeParams.leftKey,
            sizeRightKey: sizeParams.rightKey
        };

        // String -> [Num] -> [(String, Num)]
        var practiceSet = function(cond, nums){
            return _.map(nums, function(x) {return [cond, x]; });
        };

        var ptrial = function(cond, nums){
            return _.partial(trials, practiceSet(cond, [4, 7, 3, 8]), inverted, 'practice');
        };

        page('task-switching/instructions-parity.html', parityParams)();
        return waitForClick('#next')()
            .then(ptrial('parity', [4, 7, 3, 8]))
            .then(page('task-switching/instructions-size.html', sizeParams))
            .then(waitForClick('#next'))
            .then(ptrial('size', [1, 8, 6, 3]))
            .then(page('task-switching/instructions-final.html', finalParams))
            .then(waitForClick('#next'));
    };

    // TODO: add condition as a parameter
    var trials = function(stims, inverted, section){
        var stimNum = 0;
        var next = function(stims){
            var args;
            if (!stims.length) {
                return null;
            }
            return showFixation()
                .then(showStimGetResponse(stims[0], inverted))
                .then(judgeResponse)
                .then(recordData({section: 'task switching ' + section,
                                  stimulusNumber: stimNum}))
                .then(showFeedback)
                .then(function(args){
                        stimNum++;
                        return next(stims.slice(1));
                });
        };
        return next(stims);
    };

    // TaskSwitch.prototype.oddEvenExample = _.partial(
    //     TaskSwitch.prototype.examples, ['odd', 'even']);
    // TaskSwitch.prototype.smallBigExample = _.partial(
    //     TaskSwitch.prototype.examples, ['small', 'big']);
    var task = function(){
        return null;
    };

    return {
        run: run
    };
};

module.exports = {
    task: taskswitch
};
