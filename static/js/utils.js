/*global require, module */

var _ = require('underscore');

var Promise = require('es6-promise').Promise;

// utility function for functions that return promises
// var getUrl = P(function(resolve, reject){ ... });
function P(fun){
    return function(){
        return new Promise(fun);
    };
}

function log() {
    return null;
    try {
        console.log.apply(console, arguments);
    }
    catch(e) {
        try {
            opera.postError.apply(opera, arguments);
        }
        catch(e){
//            alert(Array.prototype.join.call( arguments, " "));
        }
    }
}

// A convenience method for chaining together a series of experiments.
// makeBattery(partA, partB, partC) ->
//     Promise.resolve(partA.run()).then(partB.run).then(partC.run);
// but with `this` properly bound for B. and C.run .
var makeBattery = function(){
    var parts = Array.prototype.slice.call(arguments),
        first = parts[0],
        rest = parts.slice(1);
    return rest.reduce(function(acc, item) { return acc.then(_.bind(item.run, item)); },
                       Promise.resolve(first.run()));
};

var pseq = function(){
    var parts = Array.prototype.slice.call(arguments),
        first = parts[0],
        rest = parts.slice(1);
    return function(){
        return rest.reduce(
            function(acc, item) {
                return acc.then(item);
            },
            first());
    };
};


var wait = function(ms){
    return function(args){
        return new Promise(function(resolve, reject){
            _.delay(function(){
                resolve(args);
            }, ms);
        });
    };
};

var showFixation = function(driver, text){
    log('showFixation()', 'start');
    return new Promise(function(resolve, reject){
        log('showFixation()', 'new Promise');
        _.delay(function(){
            resolve();
        }, 1000);
        driver.showPage('fixation.html');
        if (typeof text !== 'undefined'){
            $('.fixation-cross').text(text);
        };
   });
};

// returns a promise that resolves when the thing selected by selector is
// clicked.
var waitForClick = function(selector){
    return function(){
        log('waiting for click');
        return new Promise(function(resolve, reject){
            var handler = function(){
                log('done waiting for click');
                $(selector).unbind('click', handler);
                resolve();
            };
            $(selector).bind('click', handler);
        });
    };
};

var prefix = function(pre, coll){
    return coll.map(function(item){
        return pre.concat(item);
    });
};

var treePluck = function(pred, coll){
    // returns a list of items in object coll that satisfy pred, walks obj as a
    // recursive tree.

    if (pred(coll)){
        return [coll];
    } else if (typeof coll !== 'object'){
        return [];
    }
    else {
        return Array.prototype.concat.apply(
            [],
            _.map(coll, _.partial(treePluck, pred)));
    }
};

var isImage = function(x){
    return ((typeof x === 'string') &&
            x.toLowerCase().slice(-4) == '.jpg');
};

var preloadList = [];
var preloadImages = function(prefix, data){
    _.each(treePluck(isImage, data), function(src){
        var img = document.createElement('img');
        img.src = prefix + src;
        preloadList.push(img);
    });
};

// taken from http://stackoverflow.com/questions/11582512/how-to-get-url-parameters-with-javascript
var getURLParameter = function(name) {
  return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
};


module.exports = {
    makeBattery: makeBattery,
    log: log,
    wait: wait,
    showFixation: showFixation,
    waitForClick: waitForClick,
    P: P,
    pseq: pseq,
    prefix: prefix,
    preloadImages: preloadImages,
    getURLParameter: getURLParameter
};
