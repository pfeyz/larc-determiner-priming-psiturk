/*global require, uniqueId, adServerLoc, PsiTurk */

var _          = require('underscore'),
    Promise    = require('es6-promise').Promise,
    detprime   = require('./det-priming/task'),
    taskswitch = require('./task-switching/task'),
    mtelp      = require('./mtelp/task'),
    survey     = require('./survey/task'),
    utils      = require('./utils'),
    getURLParameter = utils.getURLParameter,
    P = utils.P;

var psiTurk = new PsiTurk(uniqueId, adServerLoc);
var instructionPages = [ "instructions/instruct-1.html" ];
var pages = instructionPages;
pages.push('fixation.html');
psiTurk.recordUnstructuredData('consentList', getURLParameter('consents'));
var preLoad = function(){
    psiTurk.preloadPages(pages);
};

var instructions = P(function(resolve, reject){
    psiTurk.doInstructions(instructionPages, resolve);
});

// mapping between condition and trial lists
var conditions = {
    0: {
        detprime: 'trial1',
        taskswitch: false
    },
    1: {
        detprime: 'control',
        taskswitch: false
    },
    2: {
        detprime: 'trial2',
        taskswitch: true
    },
    3: {
        detprime: 'control',
        taskswitch: true
    }
};

var battery = function(){
    var condition = parseInt(window.condition);
    var datalists = conditions[condition];
    utils.log('running condition', condition, ':', datalists);
    preLoad();
    psiTurk.finishInstructions();
    return utils.makeBattery(
        detprime.task(psiTurk, datalists.detprime),
        taskswitch.task(psiTurk, datalists.taskswitch),
        mtelp.task(psiTurk),
        survey.task(psiTurk)
    ).then(function(){
        psiTurk.completeHIT();
    });
};

$(window).load(function(){
    battery().catch(utils.log);
});
