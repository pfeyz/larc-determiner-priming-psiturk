/*global require, module, MediaElement */
'use strict';

var _ = require('underscore'),
    Promise = require('es6-promise').Promise,
    dataList = require('./data').dataList,
    utils = require('../utils'),
    P = utils.P,
    wait = utils.wait,
    waitForClick = utils.waitForClick,
    log = _.partial(utils.log, 'mtelp');


var audioPath = '/static/audio/';

var playAudio = function(basename){
    $('#audio-player').attr('src', '');
    $('#audio-ogg-src').attr('src',
        [audioPath, 'ogg/', basename, '.ogg'].join(''));
    $('#audio-mp3-src').attr('src',
        [audioPath, 'mp3/', basename, '.mp3'].join(''));

    return new Promise(function(resolve, reject){
        new MediaElement('audio-player', {
            // shows debug errors on screen
            enablePluginDebug: true,
            // remove or reorder to change plugin priority
            plugins: ['flash'],
            // specify to force MediaElement to use a particular video or audio type
            //        type: 'flash',
            // path to Flash and Silverlight plugins
            pluginPath: '/static/lib/mediaelement/',
            // name of flash file
            flashName: 'flashmediaelement.swf',
            // default if the <video width> is not specified
            defaultVideoWidth: 0,
            // default if the <video height> is not specified
            defaultVideoHeight: 0,
            // rate in milliseconds for Flash and Silverlight to fire the timeupdate event
            // larger number is less accurate, but less strain on plugin->JavaScript bridge
            timerRate: 250,
            // method that fires when the Flash or Silverlight object is ready
            success: function (mediaElement, domObject) {

                mediaElement.addEventListener('ended', function(e) {
                    resolve();
                }, false);

                // call the play method
                mediaElement.play();

            },
            // fires when a problem is detected
            error: function () {
                reject(new Error('Error loading audio file' + basename));
            }
        });
    });
};

var Mtelp = function(driver){
    var instructionPages = [
        'instructions/mtelp-instruct-1.html',
        'instructions/mtelp-instruct-2.html',
        'instructions/mtelp-instruct-3.html',
        'instructions/mtelp-instruct-4.html',
        'instructions/mtelp-instruct-5.html'
    ];
    var allPages = [
        'mtelp-frame.html',
        'fixation.html'
    ].concat(instructionPages);

    var audioExtension = 'ogg';
    var instructionAudio = [
        'MTELPInstruction1', 'MTELPInstruction2', 'MTELPInstruction3',
        'MTELPInstruction4', 'answer1', 'answer2'];
    var allAudio = instructionAudio.concat(_.pluck(dataList, 3));

    var getAudio = function(basename){
        return audioPath.concat(basename, audioExtension);
    };

    var instructions = function(){
        log('instructions');
        var showPage = _.bind(driver.showPage, driver);
        var page = function(path) {
            return _.partial(showPage, path);
        };
        var play = function(basename){
            return _.partial(playAudio, basename);
        };
        var showButton = function(){ $('button.continue').show(); };
        var hideButton = function(){ $('button.continue').hide(); };
        var disableResponse = function() { $('.btn').prop('disabled', true); };
        var enableResponse = function() { $('.btn').prop('disabled', false); };

        showPage('instructions/mtelp-instruct-1.html');

        var seq = utils.pseq(
            waitForClick('#next'),
            page('instructions/mtelp-instruct-2.html'),
            hideButton,
            play('MTELPInstruction1'),
            showButton,
            waitForClick('#next'),

            page('instructions/mtelp-instruct-3.html'),
            hideButton,
            disableResponse,
            play('MTELPInstruction2'),
            enableResponse,
            waitForClick('.response'),
            disableResponse,
            play('answer1'),

            page('instructions/mtelp-instruct-4.html'),
            hideButton,
            disableResponse,
            play('MTELPInstruction3'),
            enableResponse,
            waitForClick('.response'),
            disableResponse,
            play('answer2'),

            page('instructions/mtelp-instruct-5.html'),
            hideButton,
            play('MTELPInstruction4'),
            showButton,
            waitForClick('#next')
        );
        return seq();
    };

    var run = function(){
        log('run()');
        preloadPages();
//        preloadAudio();
        return instructions().then(trials);
    };

    var trials = P(function(endTrials, reject){

        var showStimulus = function(data){
            return new Promise(function(res, rej){
                log('showStimulus', data);
                driver.showPage('mtelp-frame.html');
                $('.response-container').hide();
                //playAudio(data.audioFile);
                playAudio(data.audioFile).then(_.partial(res, data));
                log('showing page for', data.audioFile);
                $('.choice-a').text('A. ' + data.captionA);
                $('.choice-b').text('B. ' + data.captionB);
                $('.choice-c').text('C. ' + data.captionC);
                $('.btn').prop('disabled', true);
                $('.response-container').show();
            });
        };

        var bindControls = function(data){
            return new Promise(function(res, rej){
                log('binding controls', data);
                $('.btn').prop('disabled', false);
                $('.response').bind('click', function(event){
                    res({data: data,
                         event: event});
                });
            });
        };

        var handleResponse = function(opts){
            log('handleResponse', opts.data, opts.event);
            log('handling response');
            var choice = opts.event.target.dataset['choice'];
            log('choice', choice);
            log('response', opts.data.correctResponse);
            driver.recordTrialData(
                _.extend(opts.data, {response: choice,
                                     section: "MTELP",
                                     accuracy: choice == opts.data.correctResponse}));
            driver.saveData();
            log('saved data');
            return opts.data;
        };

        var nextTrial = function(){
            var data = trialData();
            if (!data){
                endTrials();
            }
            else {
                Promise.resolve(data)
                    .then(showStimulus)
                    .then(bindControls)
                    .then(handleResponse)
                    .then(nextTrial);
            }
        };
        nextTrial();

    });

    var preloadPages = function(){
        driver.preloadPages(allPages);
    };

    // var preloadAudio = function() {
    //     var manifest = _.map(allAudio, function(basename){
    //         return {src: [basename, audioExtension].join('.'),
    //                 id: basename
    //                };
    //     });
    // };

    var responses = {
        '1': 'a',
        '6': 'b',
        '0': 'c'
    };

    var trialData = function(){
        if (dataList.length == 0){
            return false;
        };
        var trial = dataList.shift();
        return {
            captionA: trial[0],
            captionB: trial[1],
            captionC: trial[2],
            audioFile: trial[3],
            correctResponse: responses[trial[4]]
        };
    };

    return {
        allPages: allPages,
        run: run,
        trialData: trialData
    };
};

module.exports = {
    task: Mtelp
};
