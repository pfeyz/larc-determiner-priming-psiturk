/*global module */

module.exports =
    {
        dataList: [
            [
                "Yes, it is",
                "Yes, it was",
                "Yes, I have",
                "MTELP01",
                "6"
            ],
            [
                "No, she doesn't",
                "Mary",
                "No, she hasn't",
                "MTELP02",
                "1"
            ],
            [
                "Mother did the dishes",
                "Father did the dishes",
                "Gail did the dishes",
                "MTELP03",
                "0"
            ],
            [
                "Neither of them will sing",
                "One of them will sing",
                "Both of them will sing",
                "MTELP04",
                "1"
            ],
            [
                "Yes, we are",
                "Yes, he is",
                "Yes, they are",
                "MTELP05",
                "6"
            ],
            [
                "We went to Europe and Japan",
                "We only went to Europe",
                "We went only to Japan",
                "MTELP06",
                "1"
            ],
            [
                "For about five minutes",
                "To thank her",
                "In five minutes",
                "MTELP07",
                "6"
            ],
            [
                "Neither is good",
                "Both are good",
                "Only his is good",
                "MTELP08",
                "0"
            ],
            [
                "My brother is",
                "My brother does",
                "To my brother",
                "MTELP09",
                "1"
            ],
            [
                "There has been no snow in the past",
                "There has been less snow in the past",
                "There has been more snow in the past",
                "MTELP10",
                "1"
            ],
            [
                "Susie and Jane listened to the story",
                "Jane wrote the story",
                "Susie and Jane wrote the story",
                "MTELP11",
                "0"
            ],
            [
                "Yes, it is",
                "Yes, it has",
                "Yes, she has",
                "MTELP12",
                "6"
            ],
            [
                "Sally knew him",
                "Sally thought Jane knew him",
                "Neither Sally nor Jane knows him",
                "MTELP13",
                "1"
            ],
            [
                "No, I begin there next year",
                "No, I've finished already",
                "No, I finish there next year",
                "MTELP14",
                "0"
            ],
            [
                "He didn't think Alice would come",
                "He thought Alice would come",
                "He doesn't think Alice would come",
                "MTELP15",
                "0"
            ],
            [
                "Yes, I have",
                "Yes, it is",
                "Yes, it was",
                "MTELP16",
                "1"
            ],
            [
                "No, he doesn't",
                "My brother",
                "No, he hasn't",
                "MTELP17",
                "0"
            ],
            [
                "We went to Florida",
                "We went to California",
                "We went to Canada",
                "MTELP18",
                "6"
            ],
            [
                "Both of them eat it",
                "Neither of them eats it",
                "One of them eats it",
                "MTELP19",
                "1"
            ],
            [
                "Yes, he is",
                "Yes, they are",
                "Yes, it is",
                "MTELP20",
                "6"
            ],
            [
                "We bought only the motorcycle",
                "We bought only a car",
                "We bought a car and a motorcycle",
                "MTELP21",
                "6"
            ],
            [
                "In a few days",
                "For about a week",
                "To see my parents",
                "MTELP22",
                "1"
            ],
            [
                "He likes only one",
                "He likes neither",
                "He likes both",
                "MTELP23",
                "6"
            ],
            [
                "His sister is ",
                "His sister did ",
                "To his sister",
                "MTELP24",
                "0"
            ],
            [
                "Usually more people come",
                "Usually no people come",
                "Usually fewer people come",
                "MTELP25",
                "1"
            ],
            [
                "Sam and Mary heard it",
                "Sam wrote it",
                "Sam and Mary wrote it",
                "MTELP26",
                "6"
            ],
            [
                "Yes, it has",
                "Yes, she has",
                "Yes, it is",
                "MTELP27",
                "0"
            ],
            [
                "Mrs. Jones thought Mr. Jones knew them",
                "Neither Mr. Jones nor Mrs. Jones knew them",
                "Mrs. Jones knew them",
                "MTELP28",
                "6"
            ],
            [
                "No, I sold it",
                "No, I will get one",
                "No, I want to sell it",
                "MTELP29",
                "0"
            ],
            [
                "John didn't want the friend to come",
                "John wants the friend to come",
                "John wanted the friend to come",
                "MTELP30",
                "1"
            ],
            [
                "Yes, it was",
                "Yes, I have",
                "Yes, it is",
                "MTELP31",
                "0"
            ],
            [
                "No, he doesn't",
                "Marc",
                "No, he hasn't",
                "MTELP32",
                "6"
            ],
            [
                "His favorite class is English",
                "His favorite class is history",
                "His favorite class is math",
                "MTELP33",
                "1"
            ],
            [
                "Both boys like them",
                "Neither boy likes them",
                "One boy likes them",
                "MTELP34",
                "0"
            ],
            [
                "Yes, they did",
                "Yes, it did ",
                "Yes, he did",
                "MTELP35",
                "6"
            ],
            [
                "We went skiing and skating",
                "We only went skiing",
                "We only went skating",
                "MTELP36",
                "6"
            ],
            [
                "Her birthday",
                "In two hours",
                "For about two hours",
                "MTELP37",
                "0"
            ],
            [
                "Only one is windy",
                "Neither is windy",
                "Both are windy",
                "MTELP38",
                "0"
            ],
            [
                "To Mike",
                "Mike's",
                "Mike did",
                "MTELP39",
                "0"
            ],
            [
                "She never looked as angry",
                "She has looked more angry",
                "She never looks angry",
                "MTELP40",
                "1"
            ],
            [
                "Dave and John saw it",
                "John wrote the article",
                "Dave and John wrote the article",
                "MTELP41",
                "1"
            ],
            [
                "No, it hasn't",
                "No, he hasn't",
                "No, it isn't",
                "MTELP42",
                "6"
            ],
            [
                "Neither Bill nor Jack knows him",
                "Bill knew him",
                "Bill thought Jack knew him",
                "MTELP43",
                "0"
            ],
            [
                "No, I finished it",
                "No, I'm reading it now",
                "No, I haven't seen it",
                "MTELP44",
                "1"
            ],
            [
                "Bill wanted her to hire one",
                "Bill doesn't care if she hires one",
                "Bill didn't want her to hire one",
                "MTELP45",
                "0"
            ]
        ]
    };
