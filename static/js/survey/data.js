/*global module */

module.exports.pages = [
    {
        "instructions": "Adult Questionnaire: You may skip any questions you do not want to answer.  Please do not use the \"Enter\" key when you type.",
        "questions": [
            {"type": "single-choice", "text": "Gender", "options": ["Female", "Male"]},
            {"type": "single-choice", "text": "Handedness", "options": ["Right-handed", "Left-handed"]},
            {"type": "single-choice", "text": "Are there left-handers in your family", "options": ["Yes", "No"]},
            {"type": "free-text", "text": "How old are you"},
            {"type": "free-text", "text": "Where were you born"},
            {"type": "free-text", "text": "If you were not born in an English-speaking country, at what age did you move to an English-speaking country"},
            {"type": "free-text", "text": "If you were not born in an English-speaking country, at what age did you start learning English"},
            {"type": "single-choice", "text": "What is your level of education",
             "options": ["Primary school", "High school", "Some college", "BA or BS", "Some graduate school", "MA or MBA", "PhD, MD or JD"]}
        ]
    },
    {
        "instructions": "We would like you to tell us about your language use during different periods of your life: childhood (birth to age 12); adolescence (age 12 to age 18); current (age 18 to now) Consider each of the languages that you speak, one at a time. We would like to know about your speaking, listening, reading, and writing in each language, starting with English. Choose the number that best represents your answer.",
        "questions":[
            {"type": "free-text", "text": "At what age did you start learning English? Enter a number between 0 and your current age"},
            {"type": "radio-row",
             "text": "For English",
             "options": ["Never", "Rarely", "Sometimes", "Often", "Always"],
             "questions":  ["In childhood, how often did you speak English",
                            "In adolescence, how often did you speak English",
                            "At present, how often do you speak English",
                            "In childhood, how often did you hear English",
                            "In adolescence, how often did you hear English",
                            "At present, how often do you hear English",
                            "In childhood, how often did you read books or other materials in English",
                            "In adolescence, how often did you read books or other materials in English",
                            "At present, how often do you read books or other materials in English",
                            "In childhood, how often did you write in English",
                            "In adolescence, how often did you write in English",
                            "At present, how often do you write in English"]
            },
            {"type": "radio-row",
             "text": "In Childhood:",
             "options": ["Nonexistent", "Poor", "Fair", "Competent", "Good"],
             "questions": ["How would you rate your speaking of English",
                           "How would you rate your understanding of English?",
                           "How would you rate your reading of English?",
                           "How would you rate your writing in English?"]
            },
            {"type": "radio-row",
             "text": "In adolescence:",
             "options": ["Nonexistent", "Poor", "Fair", "Competent", "Good"],
             "questions": ["How would you rate your speaking of English",
                           "How would you rate your understanding of English?",
                           "How would you rate your reading of English?",
                           "How would you rate your writing in English?"]
            },
            {"type": "radio-row",
             "text": "At present:",
             "options": ["Nonexistent", "Poor", "Fair", "Competent", "Good"],
             "questions": ["How would you rate your speaking of English",
                           "How would you rate your understanding of English?",
                           "How would you rate your reading of English?",
                           "How would you rate your writing in English?"]
            },
            {"type": "single-choice",
             "text": "Do you speak or understand another language",
             "options": ["Yes",
                         "No (If no, please skip to question 28.)"]
            }
        ]},
    { "instructions": "",
      "questions": [
          {"type": "free-text",
           "text": "What is the second language you would like to tell us about"
          },
          {"type": "free-text",
           "text": "At what age did you start learning Language 2? Enter a number between 0 and your current age"
          },
          {"type": "radio-row",
           "text": "For language 2:",
           "options": ["Never", "Rarely", "Sometimes", "Often", "Always"],
           "questions": [
               "In childhood, how often did you speak language 2?",
               "In adolescence, how often did you speak language 2?",
               "At present, how often do you speak language 2?",
               "In childhood, how often did you hear language 2?",
               "In adolescence, how often did you hear language 2?",
               "At present, how often do you hear language 2?",
               "In childhood, how often did you read books or other materials in language 2?",
               "In adolescence, how often did you read books or other materials in language 2?",
               "At present, how often do you read books or other materials in language 2?",
               "In childhood, how often did you write in language 2?",
               "In adolescence, how often did you write in language 2?",
               "At present, how often do you write in language 2?"
           ]
          },
          {"type": "radio-row",
           "text": "In Childhood:",
           "options": ["Nonexistent", "Poor", "Fair", "Competent", "Good"],
           "questions": ["How would you rate your speaking of language 2",
                         "How would you rate your understanding of language 2?",
                         "How would you rate your reading of language 2?",
                         "How would you rate your writing in language 2?"]
          },
          {"type": "radio-row",
           "text": "In adolescence:",
           "options": ["Nonexistent", "Poor", "Fair", "Competent", "Good"],
           "questions": ["How would you rate your speaking of language 2",
                         "How would you rate your understanding of language 2?",
                         "How would you rate your reading of language 2?",
                         "How would you rate your writing in language 2?"]
          },
          {"type": "radio-row",
           "text": "At present:",
           "options": ["Nonexistent", "Poor", "Fair", "Competent", "Good"],
           "questions": ["How would you rate your speaking of language 2",
                         "How would you rate your understanding of language 2?",
                         "How would you rate your reading of language 2?",
                         "How would you rate your writing in language 2?"]
          },
          {"type": "single-choice",
           "text": "Do you speak or understand another language",
           "options": ["Yes",
                       "No (If no, please skip to question 28.)"]
          }
      ]
    },
    { "instructions": "",
      "questions": [
          {"type": "free-text",
           "text": "What is the third language you would like to tell us about"
          },
          {"type": "free-text",
           "text": "At what age did you start learning language 3? Enter a number between 0 and your current age"
          },
          {"type": "radio-row",
           "text": "For language 3:",
           "options": ["Never", "Rarely", "Sometimes", "Often", "Always"],
           "questions": [
               "In childhood, how often did you speak language 3?",
               "In adolescence, how often did you speak language 3?",
               "At present, how often do you speak language 3?",
               "In childhood, how often did you hear language 3?",
               "In adolescence, how often did you hear language 3?",
               "At present, how often do you hear language 3?",
               "In childhood, how often did you read books or other materials in language 3?",
               "In adolescence, how often did you read books or other materials in language 3?",
               "At present, how often do you read books or other materials in language 3?",
               "In childhood, how often did you write in language 3?",
               "In adolescence, how often did you write in language 3?",
               "At present, how often do you write in language 3?"
           ]
          },
          {"type": "radio-row",
           "text": "In Childhood:",
           "options": ["Nonexistent", "Poor", "Fair", "Competent", "Good"],
           "questions": ["How would you rate your speaking of language 3",
                         "How would you rate your understanding of language 3?",
                         "How would you rate your reading of language 3?",
                         "How would you rate your writing in language 3?"]
          },
          {"type": "radio-row",
           "text": "In adolescence:",
           "options": ["Nonexistent", "Poor", "Fair", "Competent", "Good"],
           "questions": ["How would you rate your speaking of language 3",
                         "How would you rate your understanding of language 3?",
                         "How would you rate your reading of language 3?",
                         "How would you rate your writing in language 3?"]
          },
          {"type": "radio-row",
           "text": "At present:",
           "options": ["Nonexistent", "Poor", "Fair", "Competent", "Good"],
           "questions": ["How would you rate your speaking of language 3",
                         "How would you rate your understanding of language 3?",
                         "How would you rate your reading of language 3?",
                         "How would you rate your writing in language 3?"]
          }
      ]
    },
    {"instructions": "",
     "questions": [
         {"type": "free-text", "text": "What language(s) do you feel most comfortable in"},
         {"type": "radio-row",
          "text": "Parent Occupation: Please put a check mark in the box that best describes your parent(s)' current occupations. First read through all of the choices, and then choose just one occupation for each of your parents. If they are currently unemployed or retired, please select their most recent occupation. Remember that you can skip any question you do not want to answer.",
          "options": [
              "Type A professional or intellectual occupation (e.g., architect, physician, scientist, college or university teacher, engineer, lawyer, economist, accountant)",
              "Type B professional or intellectual occupation (e.g., high school teacher, artist, nurse, social worker, government executive, librarian, sociologist, anthropologist)",
              "Type A managerial occupation (e.g., upper level manager, director, owner of a large firm, senior civil servant, building contractor, legislative official)",
              "Type B managerial or commercial occupation (e.g., sales representative, department manager, shop owner, small business owner, real estate agent, insurance agent)",
              "Non-managerial work (e.g., clerk, bookkeeper, salesperson, family care worker, secretary, bank teller, receptionist, stay-at-home parent)",
              "Type A manual and technical work (e.g., car mechanic, foreman, electrician, plumber, welder, broadcasting station operator, sound equipment operator)",
              "Type B manual and technical work (e.g., driver, factory worker, carpenter, baker, tailor, roofer)",
              "Type C manual and technical work (e.g., cleaner, packer, street sweeper, railway track worker, street vendor, warehouseman)",
              "Agricultural occupation (e.g., farm worker, independent farmer, forestry worker, fisherman, hunter)"
          ],
          "questions": ["Parent 1", "Parent 2"]
         },
         {"type": "free-text", "text": "If none of the above applies, please write in the occupation of your parent(s):"},
         {"type": "radio-row",
          "text": "Parents' gender",
          "options": ["Female", "Male"],
          "questions": ["Parent 1", "Parent 2"]
         },
         {"type": "single-choice",
          "text": "What is the highest level of education of Parent 1?",
          "options": [
              "Primary school",
              "High school",
              "Some college",
              "BA or BS",
              "Some graduate school",
              "MA or MBA",
              "PhD, MD or JD"
          ]
         },
         {"type": "single-choice",
          "text": "What is the highest level of education of Parent 2?",
          "options": [
              "Primary school",
              "High school",
              "Some college",
              "BA or BS",
              "Some graduate school",
              "MA or MBA",
              "PhD, MD or JD"
          ]
         }]
    },
    {"instructions": "",
     "questions": [
         {"type": "single-choice",
          "text": "How often do you use computers?",
          "options": ["Very Rarely", "Rarely", "Sometimes", "Often", "Very Often"]},
         {"type": "single-choice",
          "text": "How comfortable are you using computers?",
          "options": [ "Very Uncomfortable", "Uncomfortable", "Somewhat Comfortable", "Comfortable", "Very Comfortable"]},
         {"type": "single-choice",
          "text": "Do you know any computer programming languages?",
          "options": [ "Yes", "No (If no, please skip to question 40.)"]
         },
         {"type": "multi-choice",
          "text": "If you answered yes, which of the following computer programming languages do you know?",
          "options": [ "C", "C++", "HTML", "Java", "Javascript", "Python", "Other"]
         },
         {"type": "free-text",
          "text": "If you answered other, please specify which computer programming language(s) you know:"
         },
         {"type": "single-choice",
          "text": "In a typical week, how often do you use more than one programming language?",
          "options": [ "Never", "Rarely", "1-3 days", "3 or more days", "Every day"]
         },
         {"type": "multi-choice",
          "text": "Which operating system(s) do you use?",
          "options": [ "Mac OS X (Apple)", "Windows", "Linux", "Other"]
         },
         {"type": "free-text",
          "text": "If you answered other, please specify which operating system(s):"
         },
         {"type": "single-choice",
          "text": "In a typical week, how often do you use more than one operating system?",
          "options": [ "Never", "Rarely", "1-3 days", "3 or more days", "Every day"]
         }
     ]
    },
    {
        "instructions": "",
        "questions": [
            {"type": "single-choice",
             "text": "Do you play a musical instrument (including voice)",
             "options": [ "Yes", "No"]
            },
            {"type": "single-choice",
             "text": "If you answered yes, approximately how many hours per week do you practice",
             "options": ["0 to 2", "3 to 5", "6 to 8", "9 to 11", "12 or more"]
            },
            {"type": "single-choice",
             "text": "Do you play video games",
             "options": [ "Yes", "No"]
            },
            {"type": "single-choice",
             "text": "If you answered yes, what type of video games do you play the most",
             "options": [
                 "Role Playing Games", "Shooters/Action", "Puzzle/Word Games",
                 "Sports/Racing", "Simulation"]
            },
            {"type": "single-choice",
             "text": "If you answered yes, approximately how many hours per week do you play video games",
             "options": ["0 to 2", "3 to 5", "6 to 8", "9 to 11", "12 or more"]
            }
        ]
    }
];
