# validates data.json (same data as in data.js) to match our survey schema,

import json

def assert_keys(page_num, d, *keys):
    text = d['text']
    d = set(d.keys())
    keys = set(keys)
    assert d == keys, "expected {}, got {} in '{}' on page {}".format(
        keys, d, text, page_num)

with open('data.json') as fh:
    data = json.load(fh)
    assert len(data['pages']) == 7, 'not seven pages'
    for num, page in enumerate(data['pages']):
        page_num = num + 1
        assert 'instructions' in page, 'instructions missing from page {}'.format(page_num)
        for question in page['questions']:
            assert 'text' in question, 'text missing from {} on page {}'.format(question, page_num)
            qtext = question['text']
            assert 'type' in question, 'type missing from {} on page {}'.format(qtext, page_num)
            qtype = question['type']
            assert qtype in ['single-choice', 'multi-choice', 'radio-row',
                             'free-text'], 'unknown question type "{}" in "{}" on page {}'.format(qtype, qtext, page_num)
            if qtype == 'free-text':
                assert_keys(page_num, question, 'text', 'type')
            if qtype == 'single-choice' or qtype == 'multi-choice':
                assert_keys(page_num, question, 'text', 'type', 'options')
            elif qtype == 'radio-row':
                assert_keys(page_num, question, 'text', 'type', 'options', 'questions')
