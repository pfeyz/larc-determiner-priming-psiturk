/*global require, module, $ */
'use strict';

var _ = require('underscore'),
    Promise = require('es6-promise').Promise,
    utils = require('../utils'),
    data = require('./data'),
    P = utils.P,
    log = utils.log,
    wait = utils.wait,
    waitForClick = utils.waitForClick;

// returns a ul elem with li->input[radio]/input[checkbox]
var makeChoiceGroup = function(question, buttonType, num){
    var container = document.createElement('ul');
    _.each(question.options, function(optionText, optNum){
        var li = document.createElement('li'),
            radio = document.createElement('input'),
            label = document.createElement('label'),
            id = num + '-' + optNum;
        radio.id = id;
        radio.type = buttonType;
        radio.name = num;
        radio.value = optionText;
        label.innerHTML = optionText;
        label.for = id;
        $(label).bind('click', function(){
            if (buttonType == 'radio'){
                radio.checked = true;
            } else {
                radio.checked = !radio.checked;
            }
        });
        li.appendChild(radio);
        li.appendChild(label);
        container.appendChild(li);
    });
    return container;
};

var makeHeaderRow = function(cols){
    var thead = document.createElement('thead'),
        tr = document.createElement('tr');
    _.each(cols, function(c){
        var th = document.createElement('th');
        th.innerHTML = c;
        tr.appendChild(th);
    });
    thead.appendChild(tr);
    return thead;
};

var makeRadioRow = function(title, subquestionIndex, length){
    var tr = document.createElement('tr'),
        th = document.createElement('th');
    th.innerHTML = title;
    tr.appendChild(th);
    for(var i = 0; i < length; i++){
        var td = document.createElement('td'),
            radio = document.createElement('input');
        radio.type = 'radio';
        radio.name = [subquestionIndex, length].join('-');
        $(radio).data('num', i);
        td.appendChild(radio);
        tr.appendChild(td);
    }
    return tr;
};

var makeRadioGroup = function(question, questionIndex){
    var table = document.createElement('table'),
        tbody = document.createElement('tbody'),
        qs = question.options.slice();
    qs.unshift('');

    var headerRow = makeHeaderRow(qs);
    table.appendChild(headerRow);
    table.appendChild(tbody);
    _.each(question.questions, function(subquestion, subquestionIndex){
        var row = makeRadioRow(subquestion,
                               [questionIndex, subquestionIndex].join('-'),
                               question.options.length);
        tbody.appendChild(row);
    });

    table.className = 'table';
    return table;
};


var showPage = function(page, offset){
    $('#survey-instructions').text(page.instructions);
    _.each(page.questions, function(question, num){
        var el = document.createElement('li'),
            hr = document.createElement('hr');
        el.innerHTML = question.text;
        var child;
        if(question.type === 'free-text'){
            child = document.createElement('input');
            child.type = 'text';
            el.appendChild(child);
        } else if (question.type === 'single-choice'){
            child = makeChoiceGroup(question, 'radio', num);
        } else if (question.type === 'multi-choice'){
            child = makeChoiceGroup(question, 'checkbox', num);
        } else if (question.type === 'radio-row'){
            child = makeRadioGroup(question, num);
        }

        $(child).data('questionText', question.text);
        $(child).data('questionType', question.type);
        $(child).addClass('answer');
        $(child).attr('id', num + offset);
        el.appendChild(child);
        $('#survey-list').append(el);
    });
    return waitForClick('.continue')();
};

var recordData = function(driver){
    $('.answer').each(function(n, item){
        var qtype = $(item).data('questionType');
        var qtext, response, id;
        qtext = $(item).data('questionText'); // the header for each question li
        id = $(item).attr('id');
        log(id);
        qtext = [id, qtext].join(' - ');
        if(qtype === 'free-text'){
            response = item.value;
            driver.recordUnstructuredData(qtext, response);
        } else if(qtype === 'single-choice' || qtype === 'multi-choice'){
            response = _.pluck($(':checked', item), 'value');
            driver.recordUnstructuredData(qtext, response.join(','));
        } else if(qtype === 'radio-row'){
            var options = _.map($('thead th', item).slice(1),
                               function(x){ return x.innerHTML; });
            log(options);
            $('tbody tr', item).each(function(n, row){
                // TODO collect all these responses (?)
                var subqtext;
                subqtext = $('th', row).text();
                var checked = $('input:checked', row).data('num');
                var subResponse = '';
                if (typeof(checked) === 'number'){
                    subResponse = checked + 1;
                }
                driver.recordUnstructuredData([qtext, subqtext].join(' - ') + ' (' + options.join(', ') + ')',
                                              subResponse);
            });
        }
    });
    driver.saveData();
};

var survey = function(driver){
    driver.preloadPages(['survey/survey-frame.html']);
    var run = function(){
        driver.showPage('survey/survey-frame.html');
        // TODO: record survey data
        var index = 1;
        var allPages = function(pages){
            $(window).scrollTop(0);
            $('.continue').blur();
            $('#survey-list').attr('start', index);
            if(pages.length === 0){
                return null;
            }
            return showPage(pages[0], index).then(function(){
                recordData(driver);
                index += pages[0].questions.length;
                $('#survey-list').html('');
                return allPages(pages.slice(1));
            });
        };
        return allPages(data.pages);
    };
    return {
        run: run
    };
};

module.exports.task = survey;
